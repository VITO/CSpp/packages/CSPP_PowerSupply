﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="21008000">
	<Property Name="Alarm Database Computer" Type="Str"></Property>
	<Property Name="Alarm Database Name" Type="Str"></Property>
	<Property Name="Alarm Database Path" Type="Str"></Property>
	<Property Name="Data Lifespan" Type="UInt">0</Property>
	<Property Name="Database Computer" Type="Str"></Property>
	<Property Name="Database Name" Type="Str"></Property>
	<Property Name="Database Path" Type="Str"></Property>
	<Property Name="Enable Alarms Logging" Type="Bool">true</Property>
	<Property Name="Enable Data Logging" Type="Bool">true</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">)1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(^!!!*Q(C=\&gt;8"&lt;2MR%!813:"$A*T51;!7JA7VI";G"6V^6!P4AFJ1#^/#7F!,TN/'-(++=IC2(-TVS+O`80+:3[QDNP9VYEO]0GP@@NM_LD_\`K4&amp;2`NI`\;^0.WE\\ZH0]8D2;2'N3K6]:DK&gt;?1D(`H)2T\SFL?]Z3VP?=N,8P+3F\TE*5^ZSF/?]J3H@$PE)1^ZS*('Z'/C-?A99(2'C@%R0--T0-0D;QT0]!T0]!S0,D%]QT-]QT-]&lt;IPB':\B':\B-&gt;1GG?W1]QS0Y;.ZGK&gt;ZGK&gt;Z4"H.UQ"NMD:Q'Q1DWM6WUDT.UTR/IXG;JXG;JXF=DO:JHO:JHO:RS\9KP7E?BZT(-&amp;%]R6-]R6-]BI\C+:\C+:\C-6U54`%52*GQ$)Y1Z;&lt;3I8QJHO,R+YKH?)KH?)L(J?U*V&lt;9S$]XDE0-E4`)E4`)EDS%C?:)H?:)H?1Q&lt;S:-]S:-]S7/K3*\E3:Y%3:/;0N*A[=&lt;5+18*YW@&lt;,&lt;E^J&gt;YEO2U2;`0'WJ3R.FOM422L=]2[[,%?:KS(&amp;'PR9SVKL-7+N1CR`LB9[&amp;C97*0%OPH2-?Y_&lt;_KK,OKM4OKI$GKP&gt;I^&lt;`X,(_`U?N^MNLN&gt;L8#[8/*`0=4K&gt;YHA]RO&amp;QC0V_(\P&gt;\OUV].XR^E,Y_6Z[=@YH^5\`3`_$&gt;W.]DF`(N59`!/&lt;!-PQ!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">553680896</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="myGSI-HVSwitch2_DriverRevision" Type="Variable">
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">False</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!A(!!!!#%!A!!!!!!"!!A!-0````]!!1!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="myGSI-HVSwitch2_Error" Type="Variable">
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">False</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!U-!!!!#%!A!!!!!!"!!R!5Q&gt;798*J97ZU!!%!!#%!A!!!!!!"!!1!!!!"!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="myGSI-HVSwitch2_FirmwareRevision" Type="Variable">
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">False</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!A(!!!!#%!A!!!!!!"!!A!-0````]!!1!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="myGSI-HVSwitch2_Measure_Voltage_0" Type="Variable">
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">False</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!#%!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="myGSI-HVSwitch2_Measure_Voltage_1" Type="Variable">
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">False</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!#%!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="myGSI-HVSwitch2_Measure_Voltage_2" Type="Variable">
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">False</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!#%!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="myGSI-HVSwitch2_NumberOfChannels" Type="Variable">
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">False</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!&gt;'1!!!#%!A!!!!!!"!!5!!Q!!!1!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="myGSI-HVSwitch2_OutputEnable_0" Type="Variable">
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">False</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!:&amp;1!!!#%!A!!!!!!"!!1!)1!"!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="myGSI-HVSwitch2_OutputEnable_1" Type="Variable">
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">False</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!:&amp;1!!!#%!A!!!!!!"!!1!)1!"!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="myGSI-HVSwitch2_OutputEnable_2" Type="Variable">
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">False</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!:&amp;1!!!#%!A!!!!!!"!!1!)1!"!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="myGSI-HVSwitch2_ResourceName" Type="Variable">
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">False</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!A(!!!!#%!A!!!!!!"!!A!-0````]!!1!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="myGSI-HVSwitch2_Set_Voltage_0H" Type="Variable">
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">False</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!#%!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="myGSI-HVSwitch2_Set_Voltage_0L" Type="Variable">
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">False</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!#%!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="myGSI-HVSwitch2_Set_Voltage_1H" Type="Variable">
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">False</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!#%!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="myGSI-HVSwitch2_Set_Voltage_1L" Type="Variable">
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">False</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!#%!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="myGSI-HVSwitch2_Set_Voltage_2H" Type="Variable">
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">False</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!#%!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="myGSI-HVSwitch2_Set_Voltage_2L" Type="Variable">
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">False</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!#%!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="myGSI-HVSwitch2Proxy_Activate" Type="Variable">
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">False</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!:&amp;1!!!#%!A!!!!!!"!!1!)1!"!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="myGSI-HVSwitch2Proxy_Set_OnOff_Msg" Type="Variable">
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">False</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!M+!!!!#%!A!!!!!!"!!1!5Q!"!!!B!)!!!!!!!1!%!!!!!1!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="myGSI-HVSwitch2Proxy_Set_Voltage_Msg" Type="Variable">
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">False</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!M+!!!!#%!A!!!!!!"!!1!5Q!"!!!B!)!!!!!!!1!%!!!!!1!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="myGSI-HVSwitch2Proxy_Settings_Msg" Type="Variable">
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">False</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!M+!!!!#%!A!!!!!!"!!1!5Q!"!!!B!)!!!!!!!1!%!!!!!1!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="myGSI-HVSwitch2Proxy_WorkerActor" Type="Variable">
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">False</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!A(!!!!#%!A!!!!!!"!!A!-0````]!!1!!!!!!!!!!!!!!!!!!</Property>
	</Item>
</Library>
