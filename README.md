CS++ PowerSupply README
=======================
This is the PowerSupply package for CS++. It contains source code for all different power supplies so far integrated within CS++. Note that various power supplies
may still need additional drivers installed.

All power supply actors have inherited from the *CSPP_DCPwr.lvclass* base actor. They can use the base GUI for power supplies *CSPP_DCPwrGUI.lvclass*.

![An example GUI for the operation of a power supply within CS++](Docs/dcpwrgui_example.png)

| power supply class name | type | device driver | additional Ini-file entries |
| ----------------------- | ---- | ------------- | --------------------------- |
| CAEN                    | OPC  | no driver needed | none                     |
| GSI-HVSwitch2           | driver | [HVSwitch2](https://git.gsi.de/EE-LV/Drivers/HVSwitch2), [SDEX](https://git.gsi.de/EE-LV/Drivers/SDEX) | *GSI-HVSwitch2:GSI-HVSwitch2.HexAddresses*: CSV list with Hex addresses for different channels |
| HD-HVSwitch             | driver | [HD-Switch](https://git.gsi.de/EE-LV/Drivers/HD-Switch) | none     |
| ISEG                    | OPC  | no driver needed | none |
| LambdaGenesys           | driver | [GENser](https://git.gsi.de/EE-LV/Drivers/GENser) | *LambdaGenesys:LambdaGenesys.GenAddresses*: CSV list with addresses for different channels |
| PSConnect               | no device | no device, see description below | see below |

Required CS++ packages
----------------------

1. [**CSPP_Core**](https://git.gsi.de/EE-LV/CSPP/CSPP_Core): The core package is needed for all CS++ applications.
2. [**CSPP_DeviceBase**](https://git.gsi.de/EE-LV/CSPP/CSPP_DeviceBase): This package contains the base class for all power supplies *CSPP_DCPwr.lvclass* and a basic
GUI *CSPP_DCPwrGUI.lvclass* from which all functions of the power supply can be addressed.
3. [**CSPP_DSC**](https://git.gsi.de/EE-LV/CSPP/CSPP_DSC): This package is only mandatory for OPC based power supplies, but can also be used for all other power supplies.
Note that the LabVIEW DSC-Engine needs to be installed as well if this package should be used.

Optional CS++ packages
----------------------

1. [**CSPP_ListGUI**](https://git.gsi.de/EE-LV/CSPP/CSPP_ListGUI): The CS++ ListGUI is a GUI which can be used with all types of power supplies. The design philosophy is to
control all channels of the power supply from an Excel-like list without the need of any buttons.

![An example of the ListGUI to be used to control a power supply](Docs/listgui_example.png)

Configuration of OPC based power supplies
-----------------------------------------
OPC based power supplies like **CAEN** or **ISEG** needs the LabVIEW DSC-Engine as well as the CSPP_DSC package installed. The LabVIEW DSC-Engine is available within the
[LabVIEW Datalogging and Supervisory Control Module](https://wwww.ni.com/labview/labviewdsc/) for LabVIEW Developer environments or as a standalone runtime version. Note that the runtime version needs a special license.

To be able to communicate with these power supplies the following steps have to be followed:

1. Install the OPC server software from the manufacturer: Both software programs can be downloaded from their homepages. The configuration process is rather obvious but also described on their
web pages. If everything is configured correctly it could look like this:

![Running OPC server from ISEG](Docs/iseg_opc_server.png)

2. Create an OPC I/O server: Open the Distributed System Manager (DSM), create a process on your local machine and create an I/O server. In this window one can also set the update rate
for all variable coming from the OPC server: 

![Running OPC server from ISEG](Docs/opc_client_configuration.png)

Afterwards the power supply can already be controlled from the DSM.

3. But to control it from CS++ the PVs in the Ini-file have to be adapted accordingly. An example could look like this:

```
...
[myISEG.URLs]
ResourceName="ni.var.psp://localhost/myProcess/myISEG_ResourceName"
DriverRevision="ni.var.psp://localhost/myProcess/myISEG_DriverRevision"
FirmwareRevision="ni.var.psp://localhost/ISEG/OPC/can0/ma17/FirmwareName"
Error="ni.var.psp://localhost/myProcess/myISEG_Error"
NumberOfChannels="ni.var.psp://localhost/myProcess/myISEG_NumberOfChannels"
#For all Channels:
#Channel_0:
Get_Voltage_0="ni.var.psp://localhost/ISEG/OPC/can0/ma17/ch00/VMeas"
Get_Current_0="ni.var.psp://localhost/ISEG/OPC/can0/ma17/ch00/IMeas"
Get_OnOff_0="ni.var.psp://localhost/ISEG/OPC/can0/ma17/ch00/On"
Set_Current_0="ni.var.psp://localhost/ISEG/OPC/can0/ma17/ch00/ISet"
Set_Voltage_0="ni.var.psp://localhost/ISEG/OPC/can0/ma17/ch00/VSet"
...
```

Configuration of driver based power supplies
--------------------------------------------
The usage of driver based power supplies does not require additional programs to be installed or configured. Only the device driver package needs to be installed in the *instr.lib* sub-folder.

These power supplies also run with other *PVConnections* and *PVMonitors* than the DSC one. The *SVConnection* has proven to be a bit unreliable regarding GUI operation so we recommend the *LVMsgConnection* for the Set-PVs and
the *LVNotConnection* for the Get-PVs if the DSC engine is not available.

Configuration of *EPICS* based power supplies
--------------------------------------------
It also possible to use power supplies with a built-in *EPICS* server. An example for this are newer **ISEG** power supplies with Ethernet interfaces. In the following the installation of such a power supply will be explained.

1. As a first step the *EPICS* server needs to be activated. 

2. Afterwards one has to open the Distributed Systems Manager (DSM), create a process on the local machine and create an I/O server. The configuration of an *EPICS* client is unfortunately much more work than the OPC client from above. The reason is that one has to manually configure each variable:

![Running *EPICS* server from ISEG](Docs/epics_client_configuration.PNG)

3. First one needs the names of each variable. In the case of **ISEG** one can download a text file with all names from a web-based application called *iCS*. For each variable one needs to create a record entry. The datatype of the "VAL" field needs to be correct. The "SCAN" field is not always necessary. It controls the communication between the physical device and the *EPICS* server. Unfortunately the default values for the **ISEG** power supplies is "passive" which means that the device will never actively update the *EPICS* record by itself. For read-back variables this is has to be changed (for example to "2 second"). 

4. If this is done one can access the channels. In the case of **ISEG** there are two more problems to consider: (i) The datatype of the OnOff Write- and Read-Variables is a special string and not a boolean. For this a conversion CSPP actor can be used (**CSPP_PV2PVConverter** in the [CSPP_PVConverter](https://git.gsi.de/EE-LV/CSPP/CSPP_PVConverter)). And (ii) the "SCAN" field is always reset to "passive" once the power supply was switched off and on again. Also for this "problem" a small CSPP actor exists (**CSPP_PVWriter** in the [CSPP_Utilities package](https://git.gsi.de/EE-LV/CSPP/CSPP_Utilities)).

Create logical channels with PSConnect
--------------------------------------
The PSConnect class is a special 'abstract' power supply which let the user create logical channels and freely connect them to physical channels of a power supply. So assume a quadrupole should be controlled. It might be
advantageous to control logical channels like *Vertical_Steering*, *Horicontal_Steering* and *Focus* rather than *Electrode1* to *Electrode4*.

The configuration of a PSConnect is similar to a normal power supply with some differences:

```
...
CSPP_DCPwr:CSPP_DCPwr.ChannelNames="Vertical,Horizontal,Focus,Single"
CSPP_DCPwr:CSPP_DCPwr.ChannelMultiplier="L,H"
#Naming Convention: ConversionFormulars_"Name of Physical Channel", each column represents one logical channel
PSConnect:PSConnect.Conversion(V)_0="0.2*Vertical+0.5*Horizontal"
PSConnect:PSConnect.Conversion(V)_1="Vertical+2*Focus"
PSConnect:PSConnect.Conversion(V)_2="3*Horizontal+0.5*Focus"
PSConnect:PSConnect.Conversion(V)_3="5*Horizontal+0.5*Focus+2*Vertical"
PSConnect:PSConnect.Conversion(V)_4="Single"
PSConnect:PSConnect.StartNestedPS=True
...
```

The channel names of a PSConnect power supply are the names of the new, logical channels. In addition the PSConnect class needs information about the conversion formulas (for voltages and/or currents). The post-fix of each
"conversion"-entry reflects the name of a specific channel of the power supply to be controlled. So within the example above the controlled power supply has at least channels with the name "0", "1", "2", "3", and "4". The
different logical channels can be used within the formulas.

The entry *StartNestedPS* is also unique for a PSConnect power supply. It links the PSConnect instance with the instance of the controlled power supply. So this power supply does not have to be started and stopped additionally.

The URL section of a PSConnect class is also slightly different:

```
[myPSConnect.URLs]
ResourceName="ni.var.psp://localhost/CSPP_PowerSupply_SV/myPSConnect_ResourceName"
DriverRevision="ni.var.psp://localhost/CSPP_PowerSupply_SV/myPSConnect_DriverRevision"
FirmwareRevision="ni.var.psp://localhost/CSPP_PowerSupply_SV/myPSConnect_FirmwareRevision"
Error="ni.var.psp://localhost/CSPP_PowerSupply_SV/myPSConnect_Error"
NumberOfChannels="ni.var.psp://localhost/CSPP_PowerSupply_SV/myPSConnect_NumberOfChannels"
#Readback PVs from connected device (channel names from Resource!):
Get_Voltage_0="ni.var.psp://localhost/CSPP_PowerSupply_SV/myPSConnect_Get_Voltage_0"
Get_Voltage_1="ni.var.psp://localhost/CSPP_PowerSupply_SV/myPSConnect_Get_Voltage_1"
Get_Voltage_2="ni.var.psp://localhost/CSPP_PowerSupply_SV/myPSConnect_Get_Voltage_2"
Get_Voltage_3="ni.var.psp://localhost/CSPP_PowerSupply_SV/myPSConnect_Get_Voltage_3"
Get_Voltage_4="ni.var.psp://localhost/CSPP_PowerSupply_SV/myPSConnect_Get_Voltage_4"
#For all Channels:
#Channel_Vertical:
Set_Voltage_Vertical="ni.var.psp://localhost/CSPP_PowerSupply_SV/myPSConnect_Set_Voltage_Vertical"
Get_OnOff_Vertical="ni.var.psp://localhost/CSPP_PowerSupply_SV/myPSConnect_Get_OnOff_Vertical"
...
```

Readback-PVs like Get-Voltage or Get-Current need to be directly connected to the corresponding PVs from the controlled power supply (with its physical channel names!). Set-PVs are normally treated as PVs from PSConnect.
Note that in the example above Get-OnOff in fact counts as a Set-PV (for the explanation of this see the Readme of *CSPP_DeviceBase*).

The PSConnect actor class can not yet connect channels of different devices but also works with ListGUIs:

![ListGUI example for a PSConnect instance](Docs/listgui__psconnect_example.png)

**IMPORTANT:** If a PSConnect instance is used and the GUI of the corresponding device object is not opened, make sure to deactivate *DelayedActivtation* in the proxy of the device object. If this is not done the
device proxy will not be active and one cannot send commands to the device.

Scaling Possibilities
--------------------------------------
Many power supplies need the possibility to scale the voltage and / or current values (set and get values) because for example values are not in SI units. Since the way these scaling works for different power supplies can be
different there is more than one place in the code to adjust for that:

1. The Current and Voltage Scaling factors in the attributes of the instances can be used to modify the actual value sent to the device via the *Set Voltage Core.vi* and the *Set Current Core.vi*. This is the simplest way of scaling but works only for power supplies who uses the *Core.vis* to communicate with the device.

2. Another possibility is the override method *Scale PV Current.vi*. This method is hard-coded for each power supply and works especially for power supplies who use a direct communication over shared variables (OPC for example). Internally the actor always works with SI unit. The idea is that this overwritten VI makes the necessary conversion.

3. The first two points showed the different possibilities for sending data. The readback has to be taken care of by the corresponding GUI. For example, the **ListGUI** has a DB-entry to modify the returned current values (the example below means that for example the device publishes its values in A but the GUI shows them in mA):

```
...
CSPP_ListGui:CSPP_ListGui.HVCurrentReadbackScaler=1000
...
```

Related information
---------------------------------
Author: H.Brand@gsi.de, D.Neidherr@gsi.de

Copyright 2020  GSI Helmholtzzentrum für Schwerionenforschung GmbH

Planckstr.1, 64291 Darmstadt, Germany

Lizenziert unter der EUPL, Version 1.1 oder - sobald diese von der Europäischen Kommission genehmigt wurden - Folgeversionen der EUPL ("Lizenz"); Sie dürfen dieses Werk ausschließlich gemäß dieser Lizenz nutzen.

Eine Kopie der Lizenz finden Sie hier: http://www.osor.eu/eupl

Sofern nicht durch anwendbare Rechtsvorschriften gefordert oder in schriftlicher Form vereinbart, wird die unter der Lizenz verbreitete Software "so wie sie ist", OHNE JEGLICHE GEWÄHRLEISTUNG ODER BEDINGUNGEN - ausdrücklich oder stillschweigend - verbreitet.

Die sprachspezifischen Genehmigungen und Beschränkungen unter der Lizenz sind dem Lizenztext zu entnehmen.